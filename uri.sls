;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of r6rs-coap, a CoAP library in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; r6rs-coap is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-coap is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; CoAP URI parsing and formatting

;; Parser for the "coap" and "coaps" URI schemes. Based on § 6.1 of RFC
;; 7252 and partly RFC 3986.

(library (coap uri)
  (export
    make-coap-uri coap-uri?
    coap-uri-scheme
    coap-uri-host
    coap-uri-port
    coap-uri-path
    coap-uri-query

    parse-coap-uri
    format-coap-uri
    coap-uri->options)
  (import
    (rnrs)
    (ip-address)
    (packrat))

(define-record-type coap-uri
  (sealed #t)
  (fields scheme
          host
          port
          path
          query))

(define (default-port scheme)
  (cond ((equal? scheme "coap") "5683")
        ((equal? scheme "coaps") "5684")
        (else scheme)))

(define (percent-encode x)
  (define nibbles "0123456789ABCDEF")
  (call-with-string-output-port
    (lambda (p)
      (do ((bv (string->utf8 x))
           (i 0 (fx+ i 1)))
          ((fx=? i (bytevector-length bv)))
        (let* ((b (bytevector-u8-ref bv i))
               (c (integer->char b)))
          (cond ((or (char<=? #\a c #\z)
                     (char<=? #\A c #\Z)
                     (char<=? #\0 c #\9)
                     ;; TODO: What is the correct list of characters?
                     (memv c '(#\- #\. #\_ #\~
                               #\=)))
                 (put-char p c))
                (else
                 (let ((n0 (string-ref nibbles (fxbit-field b 4 8)))
                       (n1 (string-ref nibbles (fxbit-field b 0 4))))
                   (put-char p #\%)
                   (put-char p n0)
                   (put-char p n1)))))))))

(define (percent-decode x)
  (define (hexdigit? ch)
    (and (char? ch)
         (or (char<=? #\0 ch #\9)
             (char<=? #\a ch #\f)
             (char<=? #\A ch #\F))))
  (utf8->string
   (call-with-bytevector-output-port
     (lambda (p)
       (let ((inp (open-string-input-port x)))
         (let lp ()
           (let ((c (get-char inp)))
             (unless (eof-object? c)
               (case c
                 ((#\%)
                  (let* ((n0 (get-char inp))
                         (n1 (get-char inp)))
                    (if (and (hexdigit? n0) (hexdigit? n1))
                        (put-u8 p (string->number (string n0 n1) 16))
                        (put-u8 p (char->integer c))))) ;FIXME: reject?
                 (else
                  ;; FIXME: Should this reject bytes >#x80?
                  (put-u8 p (char->integer c))))
               (lp)))))))))

(define uri-parser
  (packrat-parser
   (begin
     (define (scheme starting-results)
       (let lp ((acc '()) (results starting-results))
         (let ((c (parse-results-token-value results)))
           (cond
             ((and (char? c) (or (char-ci<=? #\A c #\Z) (memv c '(#\+ #\- #\.))))
              (lp (cons c acc)
                  (parse-results-next results)))
             ((null? acc)
              (make-expected-result (parse-results-position starting-results)
                                    'scheme))
             (else
              (let ((s (string-downcase (list->string (reverse acc)))))
                (make-result s results)))))))
     (define (ipv4 starting-results)
       (let lp ((acc '()) (results starting-results))
         (let ((c (parse-results-token-value results)))
           (cond
             ((and (char? c) (or (char<=? #\0 c #\9) (eqv? c #\.)))
              (lp (cons c acc)
                  (parse-results-next results)))
             ((null? acc)
              (make-expected-result (parse-results-position starting-results)
                                    'ipv4))
             (else
              (let ((addr (string->ipv4 (list->string (reverse acc)))))
                (if addr
                    (make-result (ipv4->string addr) results)
                    (make-expected-result (parse-results-position starting-results)
                                          'ipv4))))))))
     (define (ipv6 starting-results)
       (let lp ((acc '()) (results starting-results))
         (let ((c (parse-results-token-value results)))
           (cond
             ((and (char? c) (not (memv c '(#\] #\/))))
              (lp (cons c acc)
                  (parse-results-next results)))
             ((null? acc)
              (make-expected-result (parse-results-position starting-results)
                                    'ipv6))
             (else
              ;; TODO: Support scope?
              (let ((addr (string->ipv6 (list->string (reverse acc)))))
                (if addr
                    (make-result (ipv6->string addr) results)
                    (make-expected-result (parse-results-position starting-results)
                                          'ipv4))))))))
     (define (hostname starting-results)
       (let lp ((acc '()) (results starting-results))
         (let ((c (parse-results-token-value results)))
           (cond
             ((and (char? c) (or (char-ci<=? #\A c #\Z) (char<=? #\0 c #\9)
                                 (eqv? c #\%)
                                 (memv c '(#\- #\. #\_ #\~
                                           #\! #\$ #\& #\' #\( #\) #\* #\+ #\, #\; #\=))))
              (lp (cons c acc)
                  (parse-results-next results)))
             ((null? acc)
              (make-expected-result (parse-results-position starting-results)
                                    'hostname))
             (else
              (let ((s (percent-decode (string-downcase (list->string (reverse acc))))))
                (make-result s results)))))))
     (define (port-number starting-results)
       (let lp ((acc '()) (results starting-results))
         (let ((c (parse-results-token-value results)))
           (cond
             ((and (char? c) (or (char<=? #\0 c #\9)))
              (lp (cons c acc) (parse-results-next results)))
             ((null? acc)
              (make-result #f results))
             (else
              (let ((addr (string->number (list->string (reverse acc)) 10)))
                (if addr
                    (make-result (number->string addr 10) results)
                    (make-expected-result (parse-results-position starting-results)
                                          'port))))))))
     (define (segment starting-results)
       (let lp ((acc '()) (results starting-results))
         (let ((c (parse-results-token-value results)))
           (cond
             ((and (char? c) (or (char-ci<=? #\A c #\Z) (char<=? #\0 c #\9)
                                 (eqv? c #\%)
                                 (memv c '(#\- #\. #\_ #\~
                                           #\! #\$ #\& #\' #\( #\) #\* #\+ #\, #\; #\=
                                           #\: #\@))))
              (lp (cons c acc)
                  (parse-results-next results)))
             (else
              (let ((s (percent-decode (list->string (reverse acc)))))
                (make-result s results)))))))
     (define (query-component starting-results)
       (let lp ((acc '()) (results starting-results))
         (let ((c (parse-results-token-value results)))
           (cond
             ((and (char? c)
                   (or (char-ci<=? #\A c #\Z) (char<=? #\0 c #\9)
                                 (eqv? c #\%)
                                 (memv c '(#\- #\. #\_ #\~
                                           #\! #\$ #\' #\( #\) #\* #\+ #\, #\; #\=
                                           #\: #\@
                                           #\/ #\?))))
              (lp (cons c acc)
                  (parse-results-next results)))
             (else
              (let ((s (percent-decode (list->string (reverse acc)))))
                (make-result s results)))))))
     expr)
   (expr         [(s <- scheme '#\: '#\/ '#\/
                     h <- host
                     pr <- port
                     pat <- path-abempty
                     q <- query
                     '#f)
                  (make-coap-uri s h (or pr (default-port s)) pat q)])
   (host         [(a <- ipv4)           a]
                 [('#\[ a <- ipv6 '#\]) a]
                 [(a <- hostname)       a])
   (port         [('#\: a <- port-number) a]
                 [() #f])
   ;; XXX: Note the difference between coap://hostname and
   ;; coap://hostname/, which should be normalized to the same.
   (path-abempty [('#\/ s <- segment s* <- path-abempty) (cons s s*)]
                 [() '()])
   (query        [('#\? q* <- query-comp*) q*]
                 [() '()])
   (query-comp*  [(q0 <- query-component '#\& q* <- query-comp*) (cons q0 q*)]
                 [(q <- query-component) (list q)]
                 [() '()])
   (nothing      [() #f])))

(define (parse-coap-uri x)
  (let ((g (packrat-string-results #f x)))
    (let ((result (uri-parser g)))
      (if (parse-result-successful? result)
          (parse-result-semantic-value result)
          (let ((e (parse-result-error result)))
            (assertion-violation 'parse-coap-uri
                                 "Parse error" x
                                 (parse-position-column (parse-error-position e))
                                 (parse-error-expected e)
                                 (parse-error-messages e)))))))

(define (format-coap-uri x)
  (call-with-string-output-port
    (lambda (p)
      (let ((scheme (coap-uri-scheme x))
            (host (coap-uri-host x))
            (port (coap-uri-port x))
            (path (coap-uri-path x))
            (query (coap-uri-query x)))
        (put-string p scheme)
        (put-string p "://")
        (cond ((string->ipv4 host)
               (put-string p host))
              ((string->ipv6 host)
               (put-char p #\[)
               (put-string p host)
               (put-char p #\]))
              (else
               (put-string p (percent-encode host))))
        (unless (and (string? port)
                     (equal? (default-port scheme) port))
          (put-char p #\:)
          (display port p))
        (for-each (lambda (component)
                    (put-char p #\/)
                    (put-string p (percent-encode component)))
                  (if (null? path) '("") path))
        (unless (null? query)
          (put-char p #\?)
          (let lp ((query query))
            (put-string p (percent-encode (car query)))
            (unless (null? (cdr query))
              (put-char p #\&)
              (lp (cdr query)))))))))

(define (coap-uri->options x)
  (let ((scheme (coap-uri-scheme x))
        (host (coap-uri-host x))
        (port (coap-uri-port x))
        (path (coap-uri-path x))
        (query (coap-uri-query x)))
    (let* ((ret (map (lambda (component)
                       `(Uri-Query . ,component))
                     query))
           (ret (if (or (null? path) (equal? path '("")))
                    ret
                    `(,@(map (lambda (component)
                               `(Uri-Path . ,component))
                             path)
                      ,@ret)))
           (ret (if (and (string? port)
                         (equal? (default-port scheme) port))
                    ret
                    `((Uri-Port . ,(string->number port))
                      ,@ret)))
           (ret (if (or (string->ipv4 host) (string->ipv6 host))
                    ret
                    `((Uri-Host . ,(coap-uri-host x))
                      ,@ret))))
      ret)))

  )