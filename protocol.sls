;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of r6rs-coap, a CoAP library in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; r6rs-coap is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-coap is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; CoAP protocol, message layer

(library (coap protocol)
  (export
    make-coap-message coap-message?
    coap-message-type
    coap-message-code
    coap-message-id
    coap-message-token
    coap-message-options
    coap-message-payload

    coap-method? coap-response?

    get-coap-message
    bytevector->coap-message

    put-coap-message
    coap-message->bytevector

    coap-message-uri-path
    coap-message-content-format)
  (import
    (rnrs)
    (coap protocol numbers)
    (coap protocol wire))

;; A slightly more user-friendly version of a CoAP message. The wire
;; library uses raw numbers for option names, the type, and so on. In
;; this record they are symbolic. User-created records can still use
;; the numeric forms, if so desired.
(define-record-type coap-message
  (sealed #t)
  (fields type code id token
          options payload))

(define (coap-method? x)
  (and (coap-message? x)
       (let ((code (coap-message-code x)))
         (or (symbol? code)
             (eqv? (car code) 0)))))

(define (coap-response? x)
  (and (coap-message? x)
       (let ((code (coap-message-code x)))
         (and (pair? code)
              (not (eqv? (car code) 0))))))

(define (bytevector->uint bv)
  (if (zero? (bytevector-length bv))
      0
      (bytevector-uint-ref bv 0 (endianness big) (bytevector-length bv))))

;; (11 . #vu8()) => (Uri-Path . "")
(define (parse-coap-option opt)
  (define who 'parse-coap-option)
  (let ((code (car opt))
        (value (cdr opt)))
    (cond ((assv code option-codes) =>
           (lambda (meaning)
             (let ((name (cadr meaning))
                   (type (caddr meaning)))
               (let ((value^
                      (case type
                        ((string) (utf8->string value))
                        ((uint) (bytevector->uint value))
                        (else value))))
                 (case name
                   ((Content-Format)
                    (cond ((assv value^ content-formats)
                           => (lambda (def)
                                (cons name (cdr def))))
                          (else (cons name value^))))
                   (else
                    (cons name value^)))))))
          (else opt))))

;; (Uri-Path . "") => (11 . ""). Converting to a bytevector is done in
;; the wire library. Numberic options are passed through unmodified.
(define (unparse-coap-option opt)
  (define who 'unparse-coap-option)
  (let ((code-name (car opt))
        (value (cdr opt)))
    (let* ((code-number
            (if (number? code-name)
                code-name
                (cond ((memp (lambda (def) (eq? (cadr def) code-name)) option-codes)
                       => caar)
                      (else (assertion-violation who "Unknown option name" opt)))))
           (value
            (cond
              ((eqv? code-number OPTION-CONTENT-FORMAT)
               (cond ((and (string? value)
                           (memp (lambda (def) (equal? (cdr def) value))
                                 content-formats))
                      => (lambda (def)
                           (caar def)))
                     ((string? value)
                      (assertion-violation who "Unknown content format" opt))
                     (else value)))
              (else value))))
      (cons code-number value))))

;; (0 . 02) => POST. Only handles method codes.
(define (parse-code code)
  (cond ((assoc code method-codes) => cdr)
        (else code)))

;; POST => (0 . 02). Only handles method codes.
(define (unparse-code code-name)
  (cond ((memp (lambda (def) (eq? (cdr def) code-name))
               method-codes)
         => caar)
        (else code-name)))

;; Read and parse a CoAP message from the port.
(define (get-coap-message port)
  (let-values ([(type code message-id token) (get-coap-header port)])
    (let* ((option* (map parse-coap-option (get-coap-options port)))
           (payload (get-coap-payload port)))
      (make-coap-message (vector-ref type-codes type)
                         (parse-code code)
                         message-id token option* payload))))

(define (bytevector->coap-message bv)
  (get-coap-message (open-bytevector-input-port bv)))

;; Format and write a CoAP message to a port. CoAP messages are
;; undelimited, so something that recognizes messages borders is
;; needed (normally UDP datagrams).
(define (put-coap-message port msg)
  (let ((type (case (coap-message-type msg)
                ((CON) TYPE-CON)
                ((NON) TYPE-NON)
                ((ACK) TYPE-ACK)
                ((RST) TYPE-RST)
                (else (coap-message-type msg))))
        (code (unparse-code (coap-message-code msg)))
        (message-id (coap-message-id msg))
        (token (coap-message-token msg))
        (options (map unparse-coap-option (coap-message-options msg))))
    (put-coap-header port type code message-id token)
    (put-bytevector port token)
    (put-coap-options port options)
    (put-coap-payload port (coap-message-payload msg))))

(define (coap-message->bytevector msg)
  (call-with-bytevector-output-port
    (lambda (p)
      (put-coap-message p msg))))

;;; Helpers

(define (coap-message-uri-path msg)
  (map cdr (filter (lambda (x) (eq? (car x) 'Uri-Path))
                   (coap-message-options msg))))

;; Gets the content format. There is no default in the protocol but
;; the application might have one.
(define (coap-message-content-format msg)
  (cond ((assq 'Content-Format (coap-message-options msg))
         => cdr)
        (else #f)))

)
