;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of r6rs-coap, a CoAP library in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; r6rs-coap is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-coap is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; CoAP assigned numbers

;; Numbers are found at:
;; https://www.iana.org/assignments/core-parameters/core-parameters.xhtml

(library (coap protocol numbers)
  (export
    TYPE-CON TYPE-NON TYPE-ACK TYPE-RST
    type-codes

    EMPTY-CODE

    METHOD-CODE-GET
    METHOD-CODE-POST
    METHOD-CODE-PUT
    METHOD-CODE-DELETE
    METHOD-CODE-FETCH
    METHOD-CODE-PATCH
    METHOD-CODE-iPATCH
    method-codes

    RESPONSE-CODE-CREATED
    RESPONSE-CODE-DELETED
    RESPONSE-CODE-VALID
    RESPONSE-CODE-CHANGED
    RESPONSE-CODE-CONTENT
    RESPONSE-CODE-CONTINUE
    RESPONSE-CODE-BAD-REQUEST
    RESPONSE-CODE-UNAUTHORIZED
    RESPONSE-CODE-BAD-OPTION
    RESPONSE-CODE-FORBIDDEN
    RESPONSE-CODE-NOT-FOUND
    RESPONSE-CODE-METHOD-NOT-ALLOWED
    RESPONSE-CODE-NOT-ACCEPTABLE
    RESPONSE-CODE-REQUEST-ENTITY-INCOMPLETE
    RESPONSE-CODE-CONFLICT
    RESPONSE-CODE-PRECONDITION-FAILED
    RESPONSE-CODE-REQUEST-ENTITY-TOO-LARGE
    RESPONSE-CODE-UNSUPPORTED-CONTENT-FORMAT
    RESPONSE-CODE-UNPROCESSABLE-ENTITY
    RESPONSE-CODE-TOO-MANY-REQUESTS
    RESPONSE-CODE-INTERNAL-SERVER-ERROR
    RESPONSE-CODE-NOT-IMPLEMENTED
    RESPONSE-CODE-BAD-GATEWAY
    RESPONSE-CODE-SERVICE-UNAVAILABLE
    RESPONSE-CODE-GATEWAY-TIMEOUT
    RESPONSE-CODE-PROXYING-NOT-SUPPORTED
    RESPONSE-CODE-HOP-LIMIT-REACHED

    OPTION-IF-MATCH
    OPTION-URI-HOST
    OPTION-ETAG
    OPTION-IF-NONE-MATCH
    OPTION-OBSERVE
    OPTION-URI-PORT
    OPTION-LOCATION-PATH
    OPTION-OSCORE
    OPTION-URI-PATH
    OPTION-CONTENT-FORMAT
    OPTION-MAX-AGE
    OPTION-URI-QUERY
    OPTION-HOP-LIMIT
    OPTION-ACCEPT
    OPTION-LOCATION-QUERY
    OPTION-BLOCK2
    OPTION-BLOCK1
    OPTION-SIZE2
    OPTION-PROXY-URI
    OPTION-PROXY-SCHEME
    OPTION-SIZE1
    OPTION-NO-RESPONSE
    option-codes

    content-formats)
  (import
    (only (rnrs) define quote))

(define TYPE-CON 0)
(define TYPE-NON 1)
(define TYPE-ACK 2)
(define TYPE-RST 3)
(define type-codes '#(CON NON ACK RST))

(define EMPTY-CODE '(0 . 00))

(define METHOD-CODE-GET    '(0 . 01))
(define METHOD-CODE-POST   '(0 . 02))
(define METHOD-CODE-PUT    '(0 . 03))
(define METHOD-CODE-DELETE '(0 . 04))
(define METHOD-CODE-FETCH  '(0 . 05))
(define METHOD-CODE-PATCH  '(0 . 06))
(define METHOD-CODE-iPATCH '(0 . 07))

(define method-codes
  '(((0 . 01) . GET)
    ((0 . 02) . POST)
    ((0 . 03) . PUT)
    ((0 . 04) . DELETE)
    ((0 . 05) . FETCH)
    ((0 . 06) . PATCH)
    ((0 . 07) . iPATCH)))

(define RESPONSE-CODE-CREATED                     '(2 . 01))
(define RESPONSE-CODE-DELETED                     '(2 . 02))
(define RESPONSE-CODE-VALID                       '(2 . 03))
(define RESPONSE-CODE-CHANGED                     '(2 . 04))
(define RESPONSE-CODE-CONTENT                     '(2 . 05))
(define RESPONSE-CODE-CONTINUE                    '(2 . 31))
(define RESPONSE-CODE-BAD-REQUEST                 '(4 . 00))
(define RESPONSE-CODE-UNAUTHORIZED                '(4 . 01))
(define RESPONSE-CODE-BAD-OPTION                  '(4 . 02))
(define RESPONSE-CODE-FORBIDDEN                   '(4 . 03))
(define RESPONSE-CODE-NOT-FOUND                   '(4 . 04))
(define RESPONSE-CODE-METHOD-NOT-ALLOWED          '(4 . 05))
(define RESPONSE-CODE-NOT-ACCEPTABLE              '(4 . 06))
(define RESPONSE-CODE-REQUEST-ENTITY-INCOMPLETE   '(4 . 08))
(define RESPONSE-CODE-CONFLICT                    '(4 . 09))
(define RESPONSE-CODE-PRECONDITION-FAILED         '(4 . 12))
(define RESPONSE-CODE-REQUEST-ENTITY-TOO-LARGE    '(4 . 13))
(define RESPONSE-CODE-UNSUPPORTED-CONTENT-FORMAT  '(4 . 15))
(define RESPONSE-CODE-UNPROCESSABLE-ENTITY        '(4 . 22))
(define RESPONSE-CODE-TOO-MANY-REQUESTS           '(4 . 29))
(define RESPONSE-CODE-INTERNAL-SERVER-ERROR       '(5 . 00))
(define RESPONSE-CODE-NOT-IMPLEMENTED             '(5 . 01))
(define RESPONSE-CODE-BAD-GATEWAY                 '(5 . 02))
(define RESPONSE-CODE-SERVICE-UNAVAILABLE         '(5 . 03))
(define RESPONSE-CODE-GATEWAY-TIMEOUT             '(5 . 04))
(define RESPONSE-CODE-PROXYING-NOT-SUPPORTED      '(5 . 05))
(define RESPONSE-CODE-HOP-LIMIT-REACHED           '(5 . 08))

(define OPTION-IF-MATCH       1)
(define OPTION-URI-HOST       3)
(define OPTION-ETAG           4)
(define OPTION-IF-NONE-MATCH  5)
(define OPTION-OBSERVE        6)
(define OPTION-URI-PORT       7)
(define OPTION-LOCATION-PATH  8)
(define OPTION-OSCORE         9)
(define OPTION-URI-PATH       11)
(define OPTION-CONTENT-FORMAT 12)
(define OPTION-MAX-AGE        14)
(define OPTION-URI-QUERY      15)
(define OPTION-HOP-LIMIT      16)
(define OPTION-ACCEPT         17)
(define OPTION-LOCATION-QUERY 20)
(define OPTION-BLOCK2         23)
(define OPTION-BLOCK1         27)
(define OPTION-SIZE2          28)
(define OPTION-PROXY-URI      35)
(define OPTION-PROXY-SCHEME   39)
(define OPTION-SIZE1          60)
(define OPTION-NO-RESPONSE    258)

;; XXX: Strings on the wire are UTF-8 encoded. When the Uri parts are
;; converted to a Uri they get percent-encoded as UTF-8 bytes.
;; TODO: lines with ? need confirmation.
(define option-codes
  '((1    If-Match                          opaque)
    (3    Uri-Host                          string)
    (4    ETag                              opaque)
    (5    If-None-Match                     empty)
    (6    Observe                           opaque) ;?
    (7    Uri-Port                          uint)
    (8    Location-Path                     string)
    (9    OSCORE                            opaque) ;?
    (11   Uri-Path                          string)
    (12   Content-Format                    uint)
    (14   Max-Age                           uint)
    (15   Uri-Query                         string)
    (16   Hop-Limit                         opaque) ;?
    (17   Accept                            uint)
    (20   Location-Query                    string)
    (23   Block2                            uint) ;?
    (27   Block1                            uint) ;?
    (28   Size2                             uint) ;?
    (35   Proxy-Uri                         string)
    (39   Proxy-Scheme                      string)
    (60   Size1                             uint)
    (258  No-Response                       opaque) ;?
    ))

;; Codes for OPTION-CONTENT-FORMAT. Maps codes to content-formats.
;; Note the entries that have the deflate encoding.
(define content-formats
  '((0     . "text/plain; charset=utf-8")
    (16    . "application/cose; cose-type=\"cose-encrypt0\"")
    (17    . "application/cose; cose-type=\"cose-mac0\"")
    (18    . "application/cose; cose-type=\"cose-sign1\"")
    (40    . "application/link-format")
    (41    . "application/xml")
    (42    . "application/octet-stream")
    (47    . "application/exi")
    (50    . "application/json")
    (51    . "application/json-patch+json")
    (52    . "application/merge-patch+json")
    (60    . "application/cbor")
    (61    . "application/cwt")
    (62    . "application/multipart-core")
    (63    . "application/cbor-seq")
    (96    . "application/cose; cose-type=\"cose-encrypt\"")
    (97    . "application/cose; cose-type=\"cose-mac\"")
    (98    . "application/cose; cose-type=\"cose-sign\"")
    (101   . "application/cose-key")
    (102   . "application/cose-key-set")
    (110   . "application/senml+json")
    (111   . "application/sensml+json")
    (112   . "application/senml+cbor")
    (113   . "application/sensml+cbor")
    (114   . "application/senml-exi")
    (115   . "application/sensml-exi")
    (256   . "application/coap-group+json")
    (271   . "application/dots+cbor")
    (280   . "application/pkcs7-mime; smime-type=server-generated-key")
    (281   . "application/pkcs7-mime; smime-type=certs-only")
    (282   . "application/pkcs7-mime; smime-type=CMC-Request")
    (283   . "application/pkcs7-mime; smime-type=CMC-Response")
    (284   . "application/pkcs8")
    (285   . "application/csrattrs")
    (286   . "application/pkcs10")
    (287   . "application/pkix-cert")
    (310   . "application/senml+xml")
    (311   . "application/sensml+xml")
    (320   . "application/senml-etch+json")
    (322   . "application/senml-etch+cbor")
    (432   . "application/td+json")
    (10000 . "application/vnd.ocf+cbor")
    (10001 . "application/oscore")
    (11050 . ("application/json" . "deflate"))
    (11060 . ("application/cbor" . "deflate"))
    (11542 . "application/vnd.oma.lwm2m+tlv")
    (11543 . "application/vnd.oma.lwm2m+json")
    )))
