;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of r6rs-coap, a CoAP library in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; r6rs-coap is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-coap is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; CoAP wire protocol

;; Defined in RFC 7252

(library (coap protocol wire)
  (export
    put-coap-header
    put-coap-option
    put-coap-options
    put-coap-payload

    get-coap-header
    get-coap-option
    get-coap-options
    get-coap-payload-marker
    get-coap-payload

    decode-error
    make-coap-decoding-error coap-decoding-error?)
  (import
    (rnrs)
    (coap protocol numbers))

(define COAP-VERSION #b01)

(define-condition-type &coap-decoding &error
   make-coap-decoding-error coap-decoding-error?)

(define (put-coap-header port type code message-id token)
  (let ((tkl (bytevector-length token)))
    (unless (fx<=? 0 tkl 8)
      (assertion-violation 'put-coap-header
                           "Expected a token of length 0-8"
                           port type code message-id token))
    (let ((bv (make-bytevector 4)))
      (bytevector-u8-set! bv 0 (fxior (fxarithmetic-shift-left COAP-VERSION 6)
                                      (fxarithmetic-shift-left type 4)
                                      tkl))
      (bytevector-u8-set! bv 1 (fxior (fxarithmetic-shift-left (car code) 5)
                                      (cdr code)))
      (bytevector-u16-set! bv 2 message-id (endianness big))
      (put-bytevector port bv))))

(define (uint->bytevector int)
  (if (zero? int)
      #vu8()
      (let* ((len (fxdiv (fxand -8 (fx+ 7 (bitwise-length int))) 8))
             (ret (make-bytevector len)))
        (bytevector-uint-set! ret 0 int (endianness big) (bytevector-length ret))
        ret)))

(define (encode-value who value)
  (cond ((bytevector? value) value)
        ((string? value) (string->utf8 value))
        ((and (number? value) (exact? value) (integer? value))
         (uint->bytevector value))
        (else
         (assertion-violation who "Invalid value type" value))))

(define (put-coap-option port code value last-code)
  (define MIN-0 0)
  (define MAX-0 12)
  (define MIN-1 (+ MAX-0 1))
  (define MAX-1 (+ MIN-1 #xFF))
  (define MIN-2 (+ MAX-1 1))
  (define MAX-2 (+ MIN-2 #xFFFF))
  (let* ((delta (fx- code last-code))
         (value (encode-value 'put-coap-option value))
         (value-len (bytevector-length value)))
    (unless (fx<=? MIN-0 delta MAX-2)
      (assertion-violation 'put-coap-option
                           "Delta out of range"
                           port code value last-code))
    (unless (fx<=? MIN-0 value-len MAX-2)
      (assertion-violation 'put-coap-option
                           "Value length out of range"
                           port code value last-code))
    (letrec* ((nibble
               (lambda (v)
                 (cond ((fx<=? MIN-0 v MAX-0) v)
                       ((fx<=? MIN-1 v MAX-1) 13)
                       (else                  14))))
              (put-varint
               (lambda (port v)
                 (cond ((fx<=? MIN-0 v MAX-0)
                        (values))
                       ((fx<=? MIN-1 v MAX-1)
                        (put-u8 port (fx- v MIN-1)))
                       (else
                        (let ((v^ (fx- v MIN-2)))
                          (put-u8 port (fxbit-field v^ 8 16))
                          (put-u8 port (fxbit-field v^ 0 8))))))))
      (put-u8 port (fxior (fxarithmetic-shift-left (nibble delta) 4)
                          (nibble value-len)))
      (put-varint port delta)
      (put-varint port value-len)
      (put-bytevector port value))))

(define (put-coap-options port option*)
  (let lp ((option* (list-sort (lambda (x y) (fx<? (car x) (car y)))
                               option*))
           (last-code 0))
    (unless (null? option*)
      (let ((code (caar option*)) (value (cdar option*)))
        (put-coap-option port code value last-code)
        (lp (cdr option*) code)))))

(define (put-coap-payload port payload)
  (let ((payload (encode-value 'put-coap-payload payload)))
    (unless (eqv? (bytevector-length payload) 0)
      (put-u8 port #xFF)
      (put-bytevector port payload))))

(define (decode-error who message port . irritants)
  (raise (condition
          (make-who-condition who)
          (make-message-condition message)
          (make-i/o-port-error port)
          (make-irritants-condition irritants))))

(define (get-bytevector-n* port n who)
  (let ((bv (get-bytevector-n port n)))
    (unless (and (bytevector? bv)
                 (fx=? (bytevector-length bv) n))
      (decode-error who "Early end of data" port n who))
    bv))

(define (get-u8* port who)
  (let ((b (get-u8 port)))
    (unless (fixnum? b)
      (decode-error who "Early end of data" port who))
    b))

(define (get-coap-header port)
  (define who 'get-coap-header)
  (let* ((bv (get-bytevector-n* port 4 who))
         (b0 (bytevector-u8-ref bv 0))
         (code^ (bytevector-u8-ref bv 1))
         (code (cons (fxbit-field code^ 5 8)
                     (fxbit-field code^ 0 5)))
         (message-id (bytevector-u16-ref bv 2 (endianness big))))
    (let ((ver (fxbit-field b0 6 8))
          (type (fxbit-field b0 4 6))
          (token-len (fxbit-field b0 0 4)))
      (unless (eqv? ver COAP-VERSION)
        (decode-error who "Wrong CoAP version" port ver))
      (unless (fx<=? 0 token-len 8)
        (decode-error who "Bad token length" port token-len))
      (let ((token (get-bytevector-n* port token-len who)))
        (values type code message-id token)))))

;; Get an option from the port. It does not consume the payload
;; marker.
(define (get-coap-option port last-code)
  (define who 'get-coap-option)
  (define MIN-0 0)
  (define MAX-0 12)
  (define MIN-1 (+ MAX-0 1))
  (define MAX-1 (+ MIN-1 #xFF))
  (define MIN-2 (+ MAX-1 1))
  (define MAX-2 (+ MIN-2 #xFFFF))
  (let ((b (lookahead-u8 port)))
    (if (or (eof-object? b) (eqv? b #xff))
        (values #f #f)
        (let* ((b (get-u8 port))
               (value-len^ (fxbit-field b 0 4))
               (delta^ (fxbit-field b 4 8)))
          (when (eqv? value-len^ 15)
            (decode-error who "Invalid value length" port value-len^))
          (when (eqv? delta^ 15)
            (decode-error who "Invalid delta" port delta^))
          (letrec ((get-varint
                    (lambda (port nibble)
                      (cond ((fx<=? nibble 12) nibble)
                            ((eqv? nibble 13)
                             (let ((b (get-u8* port who)))
                               (fx+ MIN-1 b)))
                            (else
                             (let* ((bv (get-bytevector-n* port 2 who))
                                    (v (bytevector-u16-ref bv 0 (endianness big))))
                               (fx+ MIN-2 v)))))))
            (let* ((value-len (get-varint port value-len^))
                   (delta (get-varint port delta^)))
              (let ((code (fx+ last-code delta))
                    (value (get-bytevector-n* port value-len who)))
                (values code value))))))))

(define (get-coap-options port)
  (let lp ((last-code 0))
    (let-values ([(code value) (get-coap-option port last-code)])
      (if (not code)
          '()
          (cons (cons code value) (lp code))))))

;; XXX: get-coap-option already positioned the port at the marker
(define (get-coap-payload-marker port)
  (define who 'get-coap-payload-marker)
  (let ((marker (get-u8 port)))
    (unless (eof-object? marker)
      (assert (eqv? marker #xFF))
      (when (port-eof? port)
        (decode-error who "Empty payload following payload marker" port)))
    marker))

(define (get-coap-payload port)
  (define who 'get-coap-payload)
  (let ((marker (get-coap-payload-marker port)))
    (if (eof-object? marker)
        #vu8()
        (get-bytevector-all port)))))
