;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of r6rs-coap, a CoAP library in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; r6rs-coap is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-coap is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; CoAP downloader

(import
  (rnrs)
  (srfi :27)
  (srfi :106 socket)
  (coap protocol)
  (coap protocol numbers)
  (coap uri))

(define (socket-recv-coap s size)
  (let ((dgram (socket-recv s size)))
    (if (eof-object? dgram)
        (eof-object)
        (bytevector->coap-message dgram))))

(define (uint->bytevector int)
  (if (zero? int)
      #vu8()
      (let* ((len (fxdiv (fxand -8 (fx+ 7 (bitwise-length int))) 8))
             (ret (make-bytevector len)))
        (bytevector-uint-set! ret 0 int (endianness big) (bytevector-length ret))
        ret)))

(define (mk-get-request uri)
  (let ((message-id (random-integer 65536))
        (token (uint->bytevector (random-integer #x100000000)))
        (options (coap-uri->options uri)))
    (make-coap-message 'CON 'GET
                       message-id token
                       options #vu8())))

(unless (= (length (command-line)) 2)
  (display "Usage: cget coap://hostname/path\n" (current-error-port))
  (exit 1))

(let ((uri (parse-coap-uri (cadr (command-line)))))
  (call-with-socket (make-client-socket (coap-uri-host uri)
                                        (coap-uri-port uri)
                                        *af-inet* *sock-dgram*)
    (lambda (p)
      (let ((msg (mk-get-request uri)))
        (display "=> ") (write msg) (newline)
        (socket-send p (coap-message->bytevector msg)))
      (let ((msg (socket-recv-coap p 65535)))
        (when (coap-message? msg)
          (display "<= ")
          (write msg)
          (newline))))))
