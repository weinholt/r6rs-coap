# (coap)

This is r6rs-coap, an implementation of the CoAP protocol in R6RS
Scheme. It a binary HTTP-alike protocol for constrained devices.
(This implementation is not suitable for constrained devices).

## Limitations

The whole thing is a work in progress, but it is sufficient to write
functional but unportable CoAP servers and clients. The main
difficulty lies in interfacing with the network.

SRFI 106 has UDP sockets, but lacks recvfrom(2), so there is no way to
know where a datagram came from. Other issues are how to handle
timeouts and multiple sockets. All this is pretty straightforward in
implementations like GNU Guile and Loko Scheme, but it does not make
for portable Scheme code. Another issue is how to integrate with a
DTLS library for coaps:// support.

So for now this library only handles the low-level (de)serialization
of messages and lacks a full client and server implementation.

## API

*To be documented.* See `demos` for an example.

Please note: servers should take precautions to not amplify a DDoS attack.

## Installation

Use [Akku.scm](https://akkuscm.org) and install into your project with
`akku install r6rs-coap`.

## Portability

## License

r6rs-coap is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

r6rs-coap is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
