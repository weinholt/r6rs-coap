#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; r6rs-coap is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-coap is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

(import
  (rnrs (6))
  (srfi :64)
  (coap protocol))

(test-begin "roundtrip")
(define msg0 (make-coap-message 'CON 'POST 42 #vu8(6 7)
                                '((Uri-Path . "rd"))
                                #vu8()))
(test-assert (coap-message->bytevector msg0))
(test-assert (bytevector->coap-message (coap-message->bytevector msg0)))
(test-assert (let ((bv (coap-message->bytevector msg0)))
               (equal? (coap-message->bytevector (bytevector->coap-message bv))
                       bv)))
(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
