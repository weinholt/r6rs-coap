#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; r6rs-coap is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-coap is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

(import
  (rnrs)
  (srfi :64)
  (coap uri))

(define tests
  '("COAP://1.2.3.4"
    "COAPS://[2001:db8::0]"
    "COAPS://[2001:db8::0]:3000"
    "coaps://example.com"
    "coaps://example.com/"
    "coaps://example.com//"
    "coaps://example.com/a/b"
    "coaps://example.com/a/b/"
    "coaps://example.com:/"
    "coaps://example.com:8000"
    "coaps://example.com:80000"
    "coaps://example.com:80000?"
    "coaps://example.com:80000?a=b"
    "coaps://example.com:80000?a=b&q=y"
    "coap://example.com:5683/~sensors/temp.xml"
    "coap://EXAMPLE.com/%7Esensors/temp.xml"
    "coap://EXAMPLE.com:/%7esensors/temp.xml"
    "coap://%65XAMPLE.com:/%7esensors/temp.xml"
    "coaps://example.com?a=b&q=y"
    "coaps://example.com/rd?ep=foo&lt=30"
    "coap://example.com/rd?ep=foo&lt=30"
    "coap://%C3%B6.example.com/%C3%B6?%C3%B6&"))

(for-each (lambda (uri)
            (write (parse-coap-uri uri)) (newline)
            (write uri)
            (display " => ")
            (write (format-coap-uri (parse-coap-uri uri)))
            (newline)
            (write (coap-uri->options (parse-coap-uri uri)))
            (newline)
            (newline))
          tests)
